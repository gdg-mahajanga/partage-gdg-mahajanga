from selenium import webdriver
from webdrivermanager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from time import sleep
from keys import LIST_APIS, INPUT_SEARCH
import pprint

browser = webdriver.Chrome(ChromeDriverManager().download_and_install()[0])
browser.maximize_window()
browser.get("https://github.com/public-apis/public-apis")

listing = browser.find_elements(By.CSS_SELECTOR, LIST_APIS)

search_input = browser.find_elements(By.CSS_SELECTOR, INPUT_SEARCH)[0]
search_input.send_keys("Lynxgsm")
search_input.send_keys(Keys.ENTER)

for elem in listing:
    print(elem.text)

sleep(130)
