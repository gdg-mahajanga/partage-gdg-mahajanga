class SubClass:
    def __init__(self, action):
        self.action = action

    def train(self, x, y):
        print(self.action(x, y))
