import pyautogui

from model_usage import ModelUsage


def convert_to_keys(confidence):
    if len(confidence) > 1 and confidence[1] > 90:
        pyautogui.press("space")


model_usage = ModelUsage(convert_to_keys)
model_usage.start()
