import requests
from bs4 import BeautifulSoup
import json

BASE_URL: str = "https://poesam.orange.com/fr/candidature/?fwp_paged="
suffix: str = "&fwp_sort=title_asc"

results = []

titles = {
    "nom_du_porteur_du_projet_:": "porteur_projet",
    "logo_:": "logo",
    "pays_de_déploiement_du_projet_:": "pays",
    "secteur_:": "secteur",
    "quel_problème_votre_entreprise_veut-elle_résoudre_?_:": "probleme",
    "quelle_solution_apporte_votre_entreprise_?_:": "solution",
    "décrivez_votre_projet_:": "description"
}

headers = {
    "Accept-Encoding": "gzip, deflate",
    "Content-type": "text/html; charset=UTF-8"
}


def convert_text_to_tag(text: str) -> str:
    return "_".join(text.lower().split(" "))


for i in range(100):
    print(f"Grepping page {i + 1}")
    r = requests.get(f"{BASE_URL}{i + 1}{suffix}", headers=headers)
    if r.status_code == 404:
        break

    page = BeautifulSoup(r.content, "html.parser")
    elements = page.find_all('li', class_='candidatures')

    if len(elements) == 0:
        print(f"Done! Scrapped {i + 1} pages.")
        break

    for element in elements:
        result = {}
        link = element.find('a').get('href')
        p = requests.get(link, headers=headers)
        project = BeautifulSoup(p.content, "html.parser")
        infoBody = project.find("div", {"itemprop": "articleBody"})
        info = infoBody.find_all('div', class_="mb-3")

        try:
            result['id'] = link
        except:
            print("Link is not detected")

        for indicator in info:
            title = indicator.strong.text.strip()
            value = indicator.text.replace(title, "").strip()

            try:
                if title.lower().__contains__('logo'):
                    value = indicator.find("img").get('src')

                result[titles[convert_text_to_tag(title)]] = value

            except:
                print("Couldn't find logo")

        results.append(result)

with open('result.json', 'w') as f:
    json.dump(results, f, ensure_ascii=False)
