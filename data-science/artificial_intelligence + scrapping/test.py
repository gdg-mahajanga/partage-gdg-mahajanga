from subcl import SubClass


def action(x, y):
    return x + y


s = SubClass(action)

s.train(1, 2)
