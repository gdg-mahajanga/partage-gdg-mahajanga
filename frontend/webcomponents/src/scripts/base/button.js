class MyButton extends HTMLElement {
  connectedCallback() {
    const props = {
      text: "text",
      color: "color",
      background: "background",
    };

    const defaultProps = {
      text: "Hello",
    };

    this.innerHTML = `<button class="px-4 py-2 rounded-full border-2 border-[${mainColor}] bg-[${mainColor}] text-white hover:bg-white hover:text-[${mainColor}] transition-all duration-500"></button>`;
    const button = this.querySelector("button");

    button.textContent =
      button.parentElement.attributes.getNamedItem(props.text)?.textContent ??
      defaultProps.text;

    button.addEventListener("click", this.handleClick);
  }

  handleClick(e) {
    alert("Hello World");
  }
}

window.customElements.define("my-button", MyButton);
