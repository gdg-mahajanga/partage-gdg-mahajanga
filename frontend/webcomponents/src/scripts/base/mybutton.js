const defaultProps = {
  text: "Hello World",
};

class Button extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
        <button class="px-4 py-2 rounded-full bg-[${mainColor}] text-white font-bold">
      Get Started
    </button>
        `;

    const button = this.querySelector("button");
    const type = button.parentElement.getAttribute("type");
    const text = button.parentElement.getAttribute("text");

    button.textContent = text ?? defaultProps.text;

    if (type === "outline") {
      button.classList.replace("text-white", `text-[${mainColor}]`);
      button.classList.replace(`bg-[${mainColor}]`, "bg-white");
      button.classList.add(`border-[${mainColor}]`, "border-2");
    }
  }
}

customElements.define("my-button", Button);
