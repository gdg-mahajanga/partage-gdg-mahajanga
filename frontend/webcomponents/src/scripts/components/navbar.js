class Navbar extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
    <nav class="flex justify-between px-16 py-8 items-center">
      <a href="">
        <img class="w-32" src="./src/assets/images/Vector.png" alt="" />
      </a>
      <ul class="absolute top-8 p-4 right-16 h-1/2 flex mr-64 w-1/4 justify-between bg-black text-white">
        <li><a href="#">Home</a></li>
        <li><a href="#">Courses</a></li>
        <li><a href="#">Blog</a></li>
        <li><a href="#">Pricing</a></li>
        <li class="bg-[#${mainColor}]"><a href="#">Signup</a></li>
      </ul>
      <h1 id="state">${states.count}</h1>
    </nav>
    `;

    const counter = this.querySelector("#state");

    const links = this.querySelectorAll("a");
    links.forEach((link) => {
      link.addEventListener("click", (e) => {
        counter.textContent = states.count++;
      });
    });

    this.addEventListener("slotchange", (e) => {
      console.log("Slot changed");
    });
  }
}

customElements.define("my-navbar", Navbar);
