class Card extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
            <div>
                <h4>99+ Certified mentors</h4>
            </div>
        `;
  }

  constructor(){
    super();
    this.attachShadow({mode:'open'})
  }
}

customElements.define("my-card", Card);
