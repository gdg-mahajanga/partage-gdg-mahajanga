# Flexbox

<img src="images/presentation.jpg" alt="Presentation" />


**Flexbox (Flexible Box)** est un module permettant la mise en page sur une dimension d'un conteneur(container) et de ses élements (items).

## Plan

### Terminologie
- Axes principal et secondaire
- Conteneur
- Element
### Propriétés sur le conteneur
- display
- flex-direction
- justify-content
- align-items
- align-content
- flex-wrap
- gap
- flex-flow
### Propriétés sur les élements
- flex-basis
- flex-grow
- flex-shrink
- flex
- align-self
- order
### Support par les navigateurs
### Résumé

---

## Terminologie

### Axes principal et secondaire
---

Les propriétés **flexbox** fonctionnent sur deux axes: l'axe `principal` et l'axe `secondaire`. Les deux peuvent être l'axe des abscisses ou l'axe des X et l'axe des ordonnées ou l'axe Y.

<img src="images/axis.png" alt="axis" />

### Conteneur
---
Le `conteneur` est la balise à l'intérieur de laquelle on place les élements à aligner.

### Element
---

L'`element` est une balise à l'intérieur du conteneur et qui sera aligné par les propriétés du module `flexbox`.

## Propriétes sur le conteneur
---

### display
---

La propriété **display** permet de définir un conteneur comme étant un conteneur `flexbox`. En termes simples, si vous voulez appliquez les propriétés `flexbox` sur un conteneur, vous mettez `display: flex`

```css
display: flex | inline-flex;
```

### flex-direction
---

La propriété **flex-direction** permet de définir l'axe principal selon lequel les élements vont être alignés.

```css
flex-direction: row | row-reverse | column | column-reverse
```

<img src="images/flex-direction.jpg" alt="flex-direction" />


### justify-content
---

La propriété **justify-content** perment de spécifier l'espacement des élements suivant l'axe principal.

```css
justify-content: center | space-between | space-around | space-evenly | flex-start | flex-end
```

<img src="images/justify-content.jpg" alt="justify-content" />

### align-items
---

La propriété **align-items** perment de spécifier l'espacement des élements suivant l'axe secondaire.

```css
align-items: flex-start | flex-end | center | stretch | baseline
```

<img src="images/align-items.jpg" alt="align-items" />

### align-content
---

La propriété **align-content** permet de définir un espacement entre les groupes d'élements sur l'axe secondaire. Cette propriété ne fonctionne que si `flex-wrap: wrap | wrap-reverse` et que les élements sont sur plusieurs lignes.

```css
align-content: flex-start | flex-end | center | space-between | space-around | space-evenly | stretch | start | end | baseline | first baseline | last baseline + ... safe | unsafe;
```

<img src="images/align-content.png" alt="align-content" />

### flex-wrap
---

La propriété **flex-wrap** permet de spécifier une cassure ou coupure suivant l'axe principal.

```css
flex-wrap: nowrap | wrap | wrap-reverse
```

<img src="images/flex-wrap.jpg" alt="flex-wrap" />

### gap
---

La propriété **gap** permet de définir un espacement entre les élements. Il s'agit d'un raccourci de `row-gap` et de `column-gap`. `row-gap` définit un espacement entre les lignes et `column-gap` définit un espacement entre les colomnes.

```css
row-gap: 10px;
column-gap: 30px;
// ou
gap: 10px 30px;
```
<img src="images/gap.png" alt="gap" />

### flex-flow
---

La propriété **flex-flow** est une combinaison des propriétés **flex-direction** et **flex-wrap**.

```css
flex-flow: <flex-direction> <flex-wrap>
```

Exemple:
```css
flex-flow: row nowrap | row wrap | column wrap | column wrap-reverse | ...
```

<img src="images/flex-basis__flex.jpg" alt="flex" />

## Propriétés sur les élements
### flex-basis
---

La propriété **flex-basis** permet de définir la dimension d'un élement suivant l'axe principal.

```css
flex-basis: 200px | auto | 20rem
```

<img src="images/flex-basis__flex.jpg" alt="flex-basis" />

### flex-grow
---

La propriété **flex-grow** permet de définir la dimension d'un élement relativement aux autres élements du même conteneur selon l'axe principal. `flex-grow` acroît la dimension de l'élement en question.

```css
flex-grow: 1 | 2 | 5 | ...
```

<img src="images/flex-grow.jpg" alt="flex-grow"/>

### flex-shrink
---

La propriété **flex-shrink** permet de définir la dimension d'un élement relativement aux autres élements du même conteneur selon l'axe principal mais en réduisant l'élement.

```css
flex-shrink: 1 | 3 | 2 | ...
```

<img src="images/flex-shrink.jpg" alt="flex-shrink" />

### flex
---

La propriéte **flex** est un raccourci pour les propriétés `flex-grow`, `flex-shrink` et `flex-basis`.

```css
flex: 1 2 200px | ...
```

<img src="images/flex-basis__flex.jpg" alt="flex" />

### align-self
---

La propriété **align-self** permet d'aligner un élement suivant l'axe scondaire.

```css
align-self: auto | flex-start | flex-end | center | baseline | stretch
```

<img src="images/align-self.jpg" alt="align-self"/>

### order
---

La propriété **order** permet de définir l'ordre d'un élement à l'intérieur d'un conteneur suivant l'axe principal.

```css
order: 1 | 2 | 3 | ...
```

<img src="images/order.png" alt="order" />

## Support par les navigateurs
---

<img src="images/flex-browser-support.png" alt="browser-support" />
---

<img src="images/caniuse-flex.png" alt="complete-browser-support" />

## Résumé
---

<img src="images/css-flexbox-poster.png" alt="resume" />