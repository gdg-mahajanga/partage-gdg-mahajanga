# React Hooks: les bases

## Définition

Les `Hooks` sont des fonctions qui permettent à React de disposer de certaines fonctionnalités sans avoir à écrire un composant de type classe. En effet, depuis leur introduction en 2018 avec React 16.8, il est désormais possible pour des composants de type fonction de disposer d'un état (local et global).
`To hook` veut dire `accrocher`; c'est à dire qu'ont peut prendre des fonctionnalités des composants React de type classes et les `accrocher` dans des composants de type fonctions.

# Plan

### Définition

### Règles des Hooks

### Les Hooks de bases

### Liste non exhaustive des Hooks

<br/>

## Règles des Hooks

Les `Hooks` sont des fonctions javascript mais elles obéissent aux règles suivantes:

- Elles doivent être appelées au niveau le plus élevé d'un composant. Il est `interdit` d'appeler un hook `à l'intérieur` d'une `boucle`, d'une `condition` et de `fonctions imbriquées`.
- Les Hooks doivent être appelés depuis un composant de type fonctions et de hooks personnalisés mais pas depuis une fonction Javascript régulière.

## Les Hooks de bases

---

Les `Hooks` que nous allons aborder sont:

- useState
- useEffect
- useContext
- useReducer
- useRef

Mais cette liste n'est pas exhaustive bien évidemment.

## useState

---

`useState` est un hook qui permet de gérer l'état local d'un composant React de type fonction. Il accepte un paramètre (`état initial`) et retourne deux valeurs (l'`état actuel` et une `fonction de mise à niveau` de l'état actuel).

```jsx
const [state, setState] = useState(initialState);
```

Par convention, la fonction de mise à niveau commence toujours par le mot `set` suivi du nom de l'état actuel en pascalCase.

Exemple 1:

```jsx
const [counter, setCounter] = useState(0);
```

Exemple 2:

```jsx
import React, { useState } from "react";

function Example() {
  // Declaration d'une variable d'état appelé "count"
  const [count, setCount] = useState(0);

  return (
    <div>
      <p>You clicked {count} times</p>
      <button onClick={() => setCount((count) => count + 1)}>Click me</button>
    </div>
  );
}
```

Exemple 3:

```jsx
function ExampleWithManyStates() {
  // Declaration avec de multiple variables d'états!
  const [age, setAge] = useState(42);
  const [fruit, setFruit] = useState("banana");
  const [todos, setTodos] = useState([{ text: "Learn Hooks" }]);
  // ...
}
```

## useEffect

---

`useEffect` est un hook qui permet de gérer les effets de bord d'un composant React de type fonction. Les effets de bord sont des fonctions qui risquent de modifier des variables en dehors de leur `scope` (portée) comme des variables globaux. <br/>
Il reçoit deux paramètres: un `callback` et un `array of dependecies` (tableau de dépendances).

```jsx
useEffect(callback, [dependencies]);
```

Un exemple d'effet de bord est l'appel à un API qui va charger des données depuis une source externe et risque donc de modifier des variables en sein de l'application web React. Un autre exemple est l'utilisation de timers comme `setTimeout` ou `setInterval`. Ces derniers se trouvant dans le `callback`.<br/>
Dans le `tableau de dépendances`, on écrit les dependances (`states` et/ou `props`) qui nécessitent une mise à jour de l'interface utilisateur.

Exemple 1:

```js
useEffect(() => {
  // appel d'un API
  fetch("https://fairestdb.p.rapidapi.com/friend/friendModel", {
    method: "GET",
    headers: {
      "x-rapidapi-host": "fairestdb.p.rapidapi.com",
      "x-rapidapi-key": "apikey",
    },
  })
    .then((response) => response.json())
    .then((response) => {
      // fonction de mise à niveau de l'état
      setFriends(response);
    })
    .catch((err) => {
      console.log(err);
    });
}, []);
```

Exemple 2:

```jsx
import React, { useState, useEffect } from "react";

function Example() {
  const [count, setCount] = useState(0);

  // Similaire à componentDidMount et componentDidUpdate:
  useEffect(() => {
    // Mise à jour du titre document avec l'API du navigateur
    document.title = `You clicked ${count} times`;
  });

  return (
    <div>
      <p>You clicked {count} times</p>
      <button onClick={() => setCount(count + 1)}>Click me</button>
    </div>
  );
}
```

Par défaut, React execute les effets après chaque rendu mais ce comportement peut être modifié. <br/>
On peut aussi demander à `useEffect` de supprimer certains effects une fois qu'ils sont terminés à l'instar de l'appel à un API lorsque le composant est demonté.

Exemple 3:

```jsx
import React, { useState, useEffect } from "react";

function FriendStatus(props) {
  const [isOnline, setIsOnline] = useState(null);

  function handleStatusChange(status) {
    setIsOnline(status.isOnline);
  }

  useEffect(() => {
    ChatAPI.subscribeToFriendStatus(props.friend.id, handleStatusChange);
    return () => {
      ChatAPI.unsubscribeFromFriendStatus(props.friend.id, handleStatusChange);
    };
  });

  if (isOnline === null) {
    return "Loading...";
  }

  return isOnline ? "Online" : "Offline";
}
```

Il est possible d'appeler plusieurs fois `useEffect` dans le même composant.

Exemple 4:

```jsx
function FriendStatusWithCounter(props) {
  const [count, setCount] = useState(0);

  useEffect(() => {
    document.title = `You clicked ${count} times`;
  });

  const [isOnline, setIsOnline] = useState(null);

  useEffect(() => {
    ChatAPI.subscribeToFriendStatus(props.friend.id, handleStatusChange);

  return () => {
      ChatAPI.unsubscribeFromFriendStatus(props.friend.id, handleStatusChange);
    };
  });

  function handleStatusChange(status) {
    setIsOnline(status.isOnline);
  }
  // ...
```

## useContext

`useContext` est un hook permettant de gérer l'état global d'une application React à travers l'API Context. Il reçoit comme argument
le contexte crée avec `React.createContext()` et renvoie les valeurs passées en paramètres au `<Context.Provider>` le plus proche.

Exemple 1:

```jsx
const value = useContext(MyContext);
```

Exemple 2:

```jsx
const themes = {
  light: {
    foreground: "#000000",
    background: "#eeeeee",
  },
  dark: {
    foreground: "#ffffff",
    background: "#222222",
  },
};

const ThemeContext = React.createContext(themes.light);

function App() {
  return (
    <ThemeContext.Provider value={themes.dark}>
      <Toolbar />
    </ThemeContext.Provider>
  );
}

function Toolbar(props) {
  return (
    <div>
      <ThemedButton />
    </div>
  );
}

function ThemedButton() {
  const theme = useContext(ThemeContext);
  return (
    <button style={{ background: theme.background, color: theme.foreground }}>
      I am styled by theme context!
    </button>
  );
}
```

## useReducer

---

`useReducer` est un hook qui permet de manipuler l'état local d'une application React d'une manière assez similaire à Redux et se présente
comme une alternative à `useState` lorsque les valeurs de l'état sont complexes(objets ou tableaux d'états).
Il prend trois arguments(`reducer` (fonction de type `(state, action) => newState)`, l'`état initial`, une `fonction d'initialissation de l'état`) et renvoient l'`état actuel` ainsi qu'une `fonction de mise à jour de l'état (dispatch)`.

Exemple 1:

```jsx
const [state, dispatch] = useReducer(reducer, initialArg, init);
```

Exemple 2:

```jsx
const initialState = { count: 0 };

function reducer(state, action) {
  switch (action.type) {
    case "increment":
      return { count: state.count + 1 };
    case "decrement":
      return { count: state.count - 1 };
    default:
      throw new Error();
  }
}

function Counter() {
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <>
      Count: {state.count}
      <button onClick={() => dispatch({ type: "decrement" })}>-</button>
      <button onClick={() => dispatch({ type: "increment" })}>+</button>
    </>
  );
}
```

Exemple 3:

```jsx
function init(initialCount) {
  return { count: initialCount };
}

function reducer(state, action) {
  switch (action.type) {
    case "increment":
      return { count: state.count + 1 };
    case "decrement":
      return { count: state.count - 1 };
    case "reset":
      return init(action.payload);
    default:
      throw new Error();
  }
}

function Counter({ initialCount }) {
  const [state, dispatch] = useReducer(reducer, initialCount, init);
  return (
    <>
      Count: {state.count}
      <button
        onClick={() => dispatch({ type: "reset", payload: initialCount })}
      >
        Reset
      </button>
      <button onClick={() => dispatch({ type: "decrement" })}>-</button>
      <button onClick={() => dispatch({ type: "increment" })}>+</button>
    </>
  );
}
```

## useRef

---

useRef est un hook qui permet de mettre une réference sur un élement du DOM. En effet, grâce à lui, vous pouvez manipuler le DOM dans React.

Exemple 1:

```jsx
const ref = useRef();
```

Exemple 2:

```jsx
import React, { useRef } from "react";

function App() {
  const inputRef = useRef();

  useEffect(() => {
    inputRef.current.focus();
  }, []);

  return (
    <>
      <input type="text" ref={inputRef} />
    </>
  );
}
```

## Liste non exhaustive des Hooks

- useState
- useCallback
- UseMemo
- useRef
- useImperativeHandle
- useLayoutEffect
- useDebugValue
- useDefferedValue
- useTransition
- useId<br/>
  ...................

## Documentation officielle

[React JS Official Doc](https://reactjs.org/docs/hooks-intro.html)
