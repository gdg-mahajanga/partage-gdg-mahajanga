import ApiCallUseffect from "./components/ApiCallUseffect";
import CounterUseReducer from "./components/CounterUseReducer";
import CounterUseState from "./components/CounterUseState";
import InputWithUseRef from "./components/InputWithUseRef";
import ThemeWithUseContext from "./components/ThemeWithUseContext";

function App() {
  return (
    <div>
      <CounterUseState />
      {/* <CounterUseReducer /> */}
      {/* <ApiCallUseffect /> */}
      {/* <ThemeWithUseContext /> */}
      {/* <InputWithUseRef /> */}
    </div>
  );
}

export default App;
