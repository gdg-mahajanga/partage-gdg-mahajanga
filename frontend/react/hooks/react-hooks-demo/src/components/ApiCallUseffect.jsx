import React, { useState, useEffect } from "react";

const ApiCallUseffect = () => {
  const [characters, setCharacters] = useState([]);

  useEffect(() => {
    fetch("https://rickandmortyapi.com/api/character")
      .then((response) => response.json())
      .then((chars) => setCharacters(chars.results))
      .catch((error) => console.error(error))
      .finally(() => console.log("Done"));
  }, []);

  return (
    <div>
      <h1 style={{ textAlign: "center" }}>API call with useEffect hook</h1>
      <div>
        {characters &&
          characters.map((char, index) => {
            return (
              <img key={new Date() * index} src={char.image} alt={char.image} />
            );
          })}
      </div>
    </div>
  );
};

export default ApiCallUseffect;
