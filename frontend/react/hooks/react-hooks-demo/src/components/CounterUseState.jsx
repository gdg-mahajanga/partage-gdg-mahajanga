import React, { useState } from "react";

const CounterUseState = () => {
  const [counter, setCounter] = useState(0);

  const increment = () => {
    setCounter((counter) => counter + 1);
  };

  const decrement = () => {
    setCounter((counter) => counter - 1);
  };

  const reset = () => {
    setCounter(0);
  };

  return (
    <div>
      <h1>Counter with useState Hook</h1>
      <p style={{ textAlign: "center", fontSize: "5rem" }}>{counter}</p>
      <button onClick={increment}>Increment</button>
      <button onClick={decrement}>Decrement</button>
      <button onClick={reset}>Reset</button>
    </div>
  );
};

export default CounterUseState;
