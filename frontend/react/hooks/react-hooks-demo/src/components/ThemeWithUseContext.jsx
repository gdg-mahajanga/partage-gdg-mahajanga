import React, { useContext } from "react";

const themes = {
  light: {
    foreground: "#000000",
    background: "#eeeeee",
  },
  dark: {
    foreground: "#ffffff",
    background: "#222222",
  },
};

const ThemeContext = React.createContext();

export default function ThemeWithUseContext() {
  return (
    <ThemeContext.Provider value={{ themes }}>
      <Toolbar />
    </ThemeContext.Provider>
  );
}

function Toolbar(props) {
  return (
    <div>
      <ThemedButton />
    </div>
  );
}

function ThemedButton() {
  const { themes } = useContext(ThemeContext);
  return (
    <div>
      <h1 style={{ textAlign: "center" }}>Theme with useContext hook</h1>
      <button
        style={{
          background: themes.dark.background,
          color: themes.dark.foreground,
        }}
      >
        I am styled by theme context!
      </button>
    </div>
  );
}
