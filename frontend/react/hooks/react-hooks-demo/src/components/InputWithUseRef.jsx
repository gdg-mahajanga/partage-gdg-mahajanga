import React, { useEffect, useRef } from "react";

const InputWithUseRef = () => {
  const inputRef = useRef();

  useEffect(() => {
    inputRef.current.focus();
  }, []);

  return (
    <div>
      <h1>DOM manipulation with useRef hook</h1>
      <input type="text" placeholder="Texte" ref={inputRef} />
    </div>
  );
};

export default InputWithUseRef;
