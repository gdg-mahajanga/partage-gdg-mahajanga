import React, { useReducer } from "react";

const CounterUseReducer = () => {
  const initialState = { counter: 0, likes: 0, favorites: 0 };

  const reducer = (state, action) => {
    switch (action.type) {
      case "increment":
        return { ...state, counter: state.counter + action.payload };
      case "decrement":
        return { ...state, counter: state.counter - action.payload };
      case "reset":
        return { ...state, counter: 0 };
      default:
        return state;
    }
  };

  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <div>
      <h1>Counter with useReducer Hook</h1>
      <p style={{ textAlign: "center", fontSize: "5rem" }}>{state.counter}</p>
      <button onClick={() => dispatch({ type: "increment", payload: 5 })}>
        Increment
      </button>
      <button onClick={() => dispatch({ type: "decrement", payload: 5 })}>
        Decrement
      </button>
      <button onClick={() => dispatch({ type: "reset" })}>Reset</button>
    </div>
  );
};

export default CounterUseReducer;
