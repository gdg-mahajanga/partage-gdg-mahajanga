import 'package:calculatrice/izza.dart';
import 'package:calculatrice/src/views/screens/autocomplete/autocompletion.dart';
import 'package:calculatrice/src/views/screens/backgroundchanger/backgroundchanger.dart';
import 'package:calculatrice/src/views/screens/calculatrice/calculatrice_view.dart';
import 'package:calculatrice/src/views/screens/guessnumber/guessmynumber.dart';
import 'package:calculatrice/src/views/screens/todo/todo.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Calculatrice',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const TodoScreen(),
    );
  }
}

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: Container(
        padding: const EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text("Your card"),
            const SizedBox(
              height: 16,
            ),
            Container(
              padding: const EdgeInsets.all(16),
              decoration: BoxDecoration(
                  color: Colors.cyan.shade100,
                  borderRadius: BorderRadius.circular(16)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: const [
                      Text("Alena Sybian"),
                      Icon(Icons.motion_photos_paused_rounded)
                    ],
                  ),
                  const SizedBox(height: 24),
                  const Icon(Icons.access_alarm),
                  const SizedBox(height: 8),
                  const Text("4265 4657 5769 2314"),
                  const SizedBox(height: 16),
                  const Text("12/24")
                ],
              ),
            ),
            const SizedBox(height: 16),
            SizedBox(
              width: double.infinity,
              child: OutlinedButton.icon(
                  onPressed: () {},
                  style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(16)),
                      padding: const EdgeInsets.symmetric(vertical: 12)),
                  icon: Icon(
                    Icons.add_box_outlined,
                    color: Colors.cyan.shade400,
                  ),
                  label: Text(
                    "Add New Card",
                    style: TextStyle(color: Colors.cyan.shade400),
                  )),
            ),
            const SizedBox(height: 16),
            const Text("Other payment methods"),
            const SizedBox(height: 8),
            SizedBox(
              width: double.infinity,
              height: 122,
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: List.generate(
                      12,
                      (index) => Card(
                            child: Text("Hello World"),
                          )),
                ),
              ),
            ),
            SizedBox(
              height: 154,
              child: SingleChildScrollView(
                child: Column(
                  children: List.generate(
                      12,
                      (index) => Card(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(16)),
                          child: const ListTile(
                            leading: Icon(Icons.card_giftcard),
                            title: Text("Mastercard/VISA"),
                            subtitle: Text("2454 2456 **** ****"),
                            trailing: Icon(Icons.arrow_right_alt),
                          ))).toList(),
                ),
              ),
            ),
          ],
        ),
      )),
    );
  }

  Widget card() {
    return Container(
      padding: const EdgeInsets.all(16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Icon(Icons.card_giftcard),
          Column(
            children: const [
              Text("MasterCard/VISA"),
              Text("1235 1355 **** ****"),
            ],
          ),
          IconButton(
              color: Colors.black,
              onPressed: () {},
              icon: const Icon(Icons.arrow_right_alt))
        ],
      ),
    );
  }
}
