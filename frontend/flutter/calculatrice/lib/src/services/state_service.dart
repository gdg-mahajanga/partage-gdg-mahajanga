class StateService {
  String? item;

  void setItem(String text) {
    item = text;
  }

  String getItem() => item ?? "";
}
