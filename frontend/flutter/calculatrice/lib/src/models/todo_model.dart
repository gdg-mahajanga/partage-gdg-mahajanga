class TodoModel {
  int id;
  String title;
  bool isDone;

  TodoModel(this.id, this.title, this.isDone);

  factory TodoModel.fromJSON(Map<String, dynamic> json) {
    return TodoModel(json['id'], json['title'], json['isDone']);
  }

  Map<String, dynamic> toJSON() {
    return {"id": id, "title": title, "isDone": isDone};
  }
}
