import 'package:flutter/material.dart';

Widget quantityButton() {
  return Container(
    padding: EdgeInsets.symmetric(horizontal: 6),
    decoration: BoxDecoration(
        border: Border.all(width: 1, color: Colors.grey.shade200),
        borderRadius: BorderRadius.circular(32)),
    child: Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        IconButton(onPressed: () {}, icon: const Icon(Icons.remove)),
        Text("1"),
        IconButton(onPressed: () {}, icon: const Icon(Icons.add)),
      ],
    ),
  );
}
