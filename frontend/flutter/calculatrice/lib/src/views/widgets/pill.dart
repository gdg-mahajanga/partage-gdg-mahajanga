import 'package:flutter/material.dart';

class Pills extends StatefulWidget {
  final List<String> elements;
  final Color activeColor;
  const Pills(
      {Key? key, required this.elements, this.activeColor = Colors.black})
      : super(key: key);

  @override
  State<Pills> createState() => _PillsState();
}

class _PillsState extends State<Pills> {
  int selectedIndex = 1;

  List<Widget> pillElements() {
    List<Widget> elements = [];
    for (int i = 0; i < widget.elements.length; i++) {
      if (i > 0 && i < widget.elements.length) {
        elements.add(const SizedBox(
          width: 8,
        ));
      }
      elements.add(pill(widget.elements[i], i));
    }
    return elements;
  }

  void setIndex(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Flex(
      direction: Axis.horizontal,
      children: pillElements(),
    );
  }

  Widget pill(String text, int index) {
    return Expanded(
      child: GestureDetector(
        onTap: () => setIndex(index),
        child: Container(
          alignment: Alignment.center,
          padding: const EdgeInsets.symmetric(vertical: 12),
          decoration: BoxDecoration(
              color: index == selectedIndex
                  ? widget.activeColor
                  : Colors.transparent,
              borderRadius: BorderRadius.circular(32),
              border: Border.all(color: Colors.grey.shade200)),
          child: Text(
            text,
            style: TextStyle(
                color: index == selectedIndex ? Colors.white : Colors.black),
          ),
        ),
      ),
    );
  }
}
