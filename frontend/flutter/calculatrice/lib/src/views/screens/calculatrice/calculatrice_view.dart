import 'dart:math';

import 'package:flutter/material.dart';

class Calculatrice extends StatefulWidget {
  const Calculatrice({Key? key}) : super(key: key);

  @override
  State<Calculatrice> createState() => _CalculatriceState();
}

class _CalculatriceState extends State<Calculatrice> {
  String number = "";
  String toShow = "";
  String savedNumber = "";
  String result = "";
  TextEditingController ctrl1 = TextEditingController();

  @override
  void initState() {
    super.initState();
    ctrl1.addListener(() {
      // print(ctrl1);
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            body: Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        children: [
          Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                  fit: BoxFit.contain, image: AssetImage("images/pizza.png")),
            ),
            child: Column(
              children: [
                TextField(
                  controller: ctrl1,
                  onChanged: (text) => setState(() {
                    number = text;
                  }),
                ),
                const Text("Sandwich de poulet"),
                ElevatedButton(
                    onPressed: () {
                      Random random = Random();
                      int guessnumber = random.nextInt(157);
                    },
                    child: Text("Afficher")),
                Text(number)
              ],
            ),
          )
        ],
      ),
    )));
  }

  Widget calculatriceBody() {
    return Column(
      children: [
        Text(number),
        Text(toShow),
        ElevatedButton(
            onPressed: () {
              setState(() {
                number = number.substring(0, number.length - 1);
              });
            },
            child: const Icon(Icons.arrow_left)),
        ElevatedButton(
            onPressed: () {
              setState(() {
                number = "";
                toShow = "";
                result = "";
                savedNumber = "";
              });
            },
            child: const Text("Clear")),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: GridView.count(
                reverse: true,
                crossAxisSpacing: 8,
                mainAxisSpacing: 8,
                crossAxisCount: 3,
                children: [
                  ...List.generate(
                      10,
                      (index) => ElevatedButton(
                          onPressed: () {
                            setState(() {
                              number = number + index.toString();
                            });
                          },
                          child: Text("$index"))).toList(),
                  ElevatedButton(
                      onPressed: () {
                        setState(() {
                          toShow = number + "+";
                          savedNumber = number;
                          number = "";
                        });
                      },
                      child: const Text("+")),
                  ElevatedButton(
                      onPressed: () {
                        setState(() {
                          int c = int.parse(savedNumber) + int.parse(number);
                          result = "$c";
                          toShow = toShow + number + "=" + result;
                        });
                      },
                      child: const Text("=")),
                ]),
          ),
        )
      ],
    );
  }
}

// ====================================================================

class Calcul extends StatelessWidget {
  const Calcul({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
