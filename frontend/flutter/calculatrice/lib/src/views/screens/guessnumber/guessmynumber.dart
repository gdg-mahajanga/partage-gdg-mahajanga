import 'dart:math';

import 'package:flutter/material.dart';

class GuessMyNumber extends StatefulWidget {
  const GuessMyNumber({Key? key}) : super(key: key);

  @override
  State<GuessMyNumber> createState() => _GuessMyNumberState();
}

class _GuessMyNumberState extends State<GuessMyNumber> {
  int guessedNumber = 1;
  String text = "";
  String result = "";

  @override
  void initState() {
    super.initState();
    guessedNumber = Random().nextInt(99);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(24.0),
        child: Column(
          children: [
            Text("Guess My Number $guessedNumber"),
            TextField(
              keyboardType: TextInputType.number,
              onChanged: (t) {
                setState(() {
                  text = t;
                });
              },
            ),
            ElevatedButton(
                onPressed: () {
                  String r = "";
                  if (int.parse(text) > guessedNumber) {
                    r = "C'est moins";
                  } else if (int.parse(text) < guessedNumber) {
                    r = "C'est plus";
                  } else {
                    r = "Bravoooo";
                  }

                  setState(() {
                    result = r;
                  });
                },
                child: const Text("Vérifier")),
            const SizedBox(height: 16),
            Text(result)
          ],
        ),
      ),
    ));
  }
}
