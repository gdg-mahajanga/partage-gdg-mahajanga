import 'dart:convert';

import 'package:calculatrice/src/constants/urls.dart';
import 'package:calculatrice/src/models/todo_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' show Client;

class TodoScreen extends StatefulWidget {
  const TodoScreen({Key? key}) : super(key: key);

  @override
  State<TodoScreen> createState() => _TodoScreenState();
}

class _TodoScreenState extends State<TodoScreen> {
  Client client = Client();
  String text = "";
  List<TodoModel> result = [];
  void getTodos() {
    client
        .get(Uri.parse(todoURL))
        .then((value) {
          for (var item in json.decode(value.body)) {
            setState(() {
              result.add(TodoModel.fromJSON(item));
            });
          }
        })
        .catchError((onError) => print(onError))
        .whenComplete(() => print("Completed"));
  }

  void addTodo() {
    client
        .post(Uri.parse(todoURL), body: {"title": text})
        .then((value) {
          setState(() {
            result = json.decode(value.body);
          });
        })
        .catchError((onError) => print(onError))
        .whenComplete(() => print("Completed"));
  }

  @override
  void initState() {
    super.initState();
    getTodos();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Container(
        width: double.infinity,
        padding: const EdgeInsets.all(24),
        child: Column(
          children: [
            const Text(
              "Todo App",
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
            TextField(
              onChanged: (_text) {
                setState(() {
                  text = _text;
                });
              },
              decoration: const InputDecoration(hintText: "Tâche a effectuer"),
            ),
            ElevatedButton(onPressed: addTodo, child: const Text("Ajouter")),
            ...result.map((todo) => todoItem(todo)).toList()
          ],
        ),
      ),
    ));
  }

  Widget todoItem(TodoModel todo) {
    return CheckboxListTile(
        value: todo.isDone,
        onChanged: (value) {
          setState(() {
            todo.isDone = !todo.isDone;
          });
        },
        secondary: IconButton(onPressed: () {}, icon: const Icon(Icons.delete)),
        title: Text(todo.title));
  }
}
