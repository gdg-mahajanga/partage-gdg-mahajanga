import 'package:flutter/material.dart';

class Autocompletion extends StatefulWidget {
  const Autocompletion({Key? key}) : super(key: key);

  @override
  State<Autocompletion> createState() => _AutocompletionState();
}

class _AutocompletionState extends State<Autocompletion> {
  String search = "";
  List<String> fruits = [
    "Apple",
    "Apricot",
    "Avocado",
    "Banana",
    "Bilberry",
    "Blackberry",
    "Blackcurrant",
    "Blueberry",
    "Boysenberry",
    "Cantaloupe",
    "Currant",
    "Cherry",
    "Cherimoya",
    "Cloudberry",
    "Coconut",
    "Cranberry",
    "Cucumber",
    "Damson",
    "Date",
    "Dragonfruit",
    "Durian",
    "Elderberry",
    "Feijoa",
    "Fig",
    "Goji berry",
    "Gooseberry",
    "Grape",
    "Grapefruit",
    "Guava",
    "Honeydew",
    "Huckleberry",
    "Jabouticaba",
    "Jackfruit",
    "Jambul",
    "Jujube",
    "Juniper berry",
    "Kiwi fruit",
    "Kumquat",
    "Lemon",
    "Lime",
    "Loquat",
    "Lychee",
    "Nectarine",
    "Mango",
    "Marion berry",
    "Melon",
    "Miracle fruit",
    "Mulberry",
    "Nance",
    "Orange",
    "Pamelo",
    "Papaya",
    "Passionfruit",
    "Peach",
    "Pear",
    "Persimmon",
    "Physalis",
    "Plum",
    "Pineapple",
    "Pineberry",
    "Plumcot",
    "Pomegranate",
    "Pomelo",
    "Purple Mangosteen",
    "Quince",
    "Raspberry",
    "Raisin",
    "Rambutan",
    "Redcurrant",
    "Salal berry",
    "Salak",
    "Satsuma",
    "Star fruit",
    "Strawberry",
    "Tamarillo",
    "Tamarind",
    "Ugli fruit",
    "Watermelon"
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(24.0),
        child: Column(
          children: [
            TextField(
              onChanged: (text) {
                setState(() {
                  search = text;
                });
              },
              decoration: const InputDecoration(hintText: "Search here ..."),
            ),
            Expanded(
              child: ListView(
                children: fruits
                    .where((element) => element.toLowerCase().contains(search))
                    .map((e) => ListTile(title: Text(e)))
                    .toList(),
              ),
            )
          ],
        ),
      ),
    ));
  }
}
