import 'package:flutter/material.dart';

class BackgroundChanger extends StatefulWidget {
  const BackgroundChanger({Key? key}) : super(key: key);

  @override
  State<BackgroundChanger> createState() => _BackgroundChangerState();
}

class _BackgroundChangerState extends State<BackgroundChanger> {
  List<Color> colors = [Colors.yellow, Colors.black, Colors.blue, Colors.green];
  Color actualColor = Colors.white;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Container(
            color: actualColor,
            width: double.infinity,
            child: GridView.count(
              crossAxisSpacing: 16,
              mainAxisSpacing: 16,
              crossAxisCount: 3,
              children: colors
                  .map((e) => GestureDetector(
                        onTap: () {
                          setState(() {
                            actualColor = e;
                          });
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                  width: actualColor == e ? 16 : 2,
                                  color: Colors.grey),
                              color: e),
                        ),
                      ))
                  .toList(),
            )),
      ),
    ));
  }
}
