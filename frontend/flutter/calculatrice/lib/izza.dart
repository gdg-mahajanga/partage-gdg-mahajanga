import 'package:calculatrice/src/views/widgets/pill.dart';
import 'package:calculatrice/src/views/widgets/quantity_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class IzzaScreen extends StatelessWidget {
  const IzzaScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
              leading: const Icon(
                Icons.chevron_left,
                color: Colors.black,
              ),
              actionsIconTheme: const IconThemeData(color: Colors.black),
              actions: [
                IconButton(onPressed: () {}, icon: const Icon(Icons.more_vert))
              ],
            ),
            body: Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 16.0, horizontal: 32),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: Image.asset(
                      "images/pizza.png",
                      scale: .5,
                    ),
                  ),
                  SizedBox(
                    height: 32,
                  ),
                  Text(
                    "Prosciutto e funghi",
                    style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      quantityButton(),
                      Text("€17.99"),
                    ],
                  ),
                  Text(
                      "Prosciutto e funghi Prosciutto e funghiProsciutto e funghiProsciutto e funghiProsciutto e funghiProsciutto e funghiProsciutto e funghiProsciutto e funghiProsciutto e funghiProsciutto e funghiProsciutto e funghiProsciutto e funghiProsciutto e funghiProsciutto e funghiProsciutto e funghiProsciutto e funghiProsciutto e funghiProsciutto e funghi"),
                  Pills(elements: ["24cm", "32cm", "42cm", "53cm"]),
                  Spacer(),
                  Row(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey.shade200),
                            borderRadius: BorderRadius.circular(32)),
                        child: IconButton(
                            onPressed: () {},
                            icon: Icon(Icons.favorite_border)),
                      ),
                      SizedBox(
                        width: 16,
                      ),
                      Expanded(
                          child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  padding: EdgeInsets.symmetric(vertical: 16),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(32)),
                                  elevation: 0,
                                  primary: Color(0xffFFD8B9)),
                              onPressed: () {},
                              child: Text(
                                "Add to cart",
                                style: TextStyle(color: Colors.black),
                              )))
                    ],
                  )
                ],
              ),
            )));
  }
}
