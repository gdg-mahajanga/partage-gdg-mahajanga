import 'dart:convert';

import 'package:flutter/services.dart';

class ProjectService {
  // Load sample json from file system
  Future<String> _loadAsset(String path) async {
    return rootBundle.loadString(path);
  }

  // Get the sample project json to display in ui
  Future<List> getProjects() async {
    // Simulate api request time
    await Future.delayed(const Duration(milliseconds: 1000));

    // Load json from file system
    final dataString = await _loadAsset("assets/datas/poesam.json");

    // Decode to json
    final List<dynamic> json = jsonDecode(dataString);

    return json;
  }

  Future<List> getProjectById(String id) async {
    final projects = await getProjects();

    final project = projects.where((project) => project["id"] == id).toList();

    return project;
  }

  Future<List> getProjectsByCategory(String category) async {
    final projects = await getProjects();

    final projectsByCategory =
        projects.where((project) => project["secteur"] == category).toList();

    return category == "Tous" ? projects : projectsByCategory;
  }

  Future<List> getProjectsByCountry(String country) async {
    final projects = await getProjects();

    final projectsByCountry =
        projects.where((project) => project["pays"] == country).toList();

    return country == "Tous" ? projects : projectsByCountry;
  }

  Future<List> getProjectsByFilters(String category, String country) async {
    final projects = await getProjects();
    final projectsToShow;

    if (category.isNotEmpty && country.isNotEmpty) {
      projectsToShow = projects
          .where((project) =>
              project["pays"] == country && project["secteur"] == category)
          .toList();
    } else if (category.isNotEmpty && country.isEmpty) {
      projectsToShow = await getProjectsByCategory(category);
    } else if (country.isNotEmpty && category.isEmpty) {
      projectsToShow = await getProjectsByCountry(country);
    } else {
      projectsToShow = projects;
    }

    return projectsToShow;
  }

  Future<List> getCategories() async {
    final projects = await getProjects();

    final projectsCategory = projects
        .map((project) => project["secteur"])
        .where((element) => element != null)
        .toSet()
        .toList();

    return projectsCategory;
  }

  Future<List> getCountries() async {
    final projects = await getProjects();

    final projectsCountry = projects
        .map((project) => project["pays"])
        .where((element) => element != null)
        .toSet()
        .toList();

    return projectsCountry;
  }
}
