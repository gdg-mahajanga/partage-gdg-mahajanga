class Project {
  String id;
  String logo;
  String porteurProjet;
  String pays;
  String secteur;
  String probleme;
  String solution;
  String description;

  Project({
    required this.id,
    required this.logo,
    required this.porteurProjet,
    required this.pays,
    required this.secteur,
    required this.probleme,
    required this.solution,
    required this.description,
  });

  factory Project.fromJson(Map<String, dynamic> json) {
    return Project(
      id: json["id"] as String,
      logo: json["logo"] as String,
      porteurProjet: json["porteur_projet"] as String,
      pays: json["pays"] as String,
      secteur: json["secteur"] as String,
      probleme: json["probleme"] as String,
      solution: json["solution"] as String,
      description: json["description"] as String,
    );
  }
}
