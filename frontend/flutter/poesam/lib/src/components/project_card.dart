import 'package:flutter/material.dart';
import 'package:poesam/src/api/poesam_projects_service.dart';
import 'package:poesam/src/helpers/navigation_helper.dart';
import 'package:poesam/src/views/screens/project_detail_screen.dart';

class ProjectCard extends StatefulWidget {
  final project;

  const ProjectCard({Key? key, required this.project}) : super(key: key);

  @override
  State<ProjectCard> createState() => _ProjectCardState();
}

class _ProjectCardState extends State<ProjectCard> {
  final service = ProjectService();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final project = service.getProjectById(widget.project["id"]);

        project.then(
          (value) {
            goTo(
              context,
              ProjectDetailScreen(
                project: value[0],
              ),
            );
          },
        );
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Center(child: Image.asset("assets/images/logo_placeholder.png")),
          Text(
            widget.project["porteur_projet"].toString(),
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.grey.shade800,
              fontSize: 16,
            ),
          ),
          Text(
            widget.project["pays"].toString(),
            style: TextStyle(
              color: Colors.grey.shade600,
              fontWeight: FontWeight.w700,
              fontStyle: FontStyle.italic,
            ),
          ),
          Text(
            widget.project["secteur"].toString(),
            style: const TextStyle(
              color: Colors.orange,
              fontWeight: FontWeight.w500,
            ),
          ),
          Text.rich(
            TextSpan(
              text: "Problème: ",
              style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  decoration: TextDecoration.underline),
              children: [
                TextSpan(
                  text: widget.project["probleme"].toString(),
                  style: const TextStyle(
                    decoration: TextDecoration.none,
                    fontWeight: FontWeight.normal,
                    height: 1.5,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
