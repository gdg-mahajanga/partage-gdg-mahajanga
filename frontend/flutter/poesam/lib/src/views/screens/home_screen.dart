import 'package:flutter/material.dart';
import 'package:poesam/src/api/poesam_projects_service.dart';
import 'package:poesam/src/components/projects_list_view.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final service = ProjectService();
  List categories = [];
  List countries = [];
  String dropdownValue = "Culture";
  String dropdownValueCountry = "Tunisie";
  String category = "";
  String country = "";
  int nbOfProjects = 0;

  @override
  void initState() {
    service.getCategories().then((value) {
      setState(() {
        categories = value;
      });
    });

    service.getCountries().then((value) {
      setState(() {
        countries = value;
      });
    });

    service.getProjects().then((value) {
      setState(() {
        nbOfProjects = value.length;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 24.0, vertical: 16),
              child: Column(
                children: [
                  Text(
                    "Nb of projects: $nbOfProjects",
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 24,
                    ),
                  ),
                  SizedBox(
                    height: 32,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: [
                        DropdownButton(
                          value: dropdownValue,
                          items: categories.map((category) {
                            return DropdownMenuItem(
                              value: category,
                              child: Text(category),
                            );
                          }).toList(),
                          onChanged: (text) {
                            setState(() {
                              dropdownValue = text.toString();
                              category = text.toString();

                              service
                                  .getProjectsByFilters(category, country)
                                  .then((value) {
                                setState(() {
                                  nbOfProjects = value.length;
                                });
                              });
                            });
                          },
                        ),
                        DropdownButton(
                          value: dropdownValueCountry,
                          items: countries.map((country) {
                            return DropdownMenuItem(
                              value: country,
                              child: Text(country),
                            );
                          }).toList(),
                          onChanged: (text) {
                            setState(() {
                              dropdownValueCountry = text.toString();
                              country = text.toString();
                              service
                                  .getProjectsByFilters(category, country)
                                  .then((value) {
                                setState(() {
                                  nbOfProjects = value.length;
                                });
                              });

                              //print(nbOfProjects);
                            });
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            FutureBuilder(
              future: service.getProjectsByFilters(category, country),
              builder: (context, AsyncSnapshot<List> snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  return ProjectsListView(projects: snapshot.data ?? []);
                } else {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }
              },
            )
          ],
        ),
      ),
    );
  }
}
