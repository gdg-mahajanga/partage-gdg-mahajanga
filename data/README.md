# POESAM Data

This zip file contains a json file with all projects listed on [Orange poesam website](https://poesam.orange.com/fr/candidature)

The json is formatted like this:

```json
{
  "id": "link of the project",
  "logo": "logo of the project",
  "pays": "country where the project emerged",
  "secteur": "market sector",
  "probleme": "Discovered problem to be solved (under section 'Quel problème votre entreprise veut-elle résoudre ?')",
  "solution": "Solution (under section 'Quelle solution apporte votre entreprise ?')",
  "description": "Description of the project (under section 'Décrivez votre projet')"
}
```
