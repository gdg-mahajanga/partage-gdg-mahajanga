const ws = new WebSocket("ws://192.168.70.148:4000");
const ul = document.getElementById("movieList");
ws.onopen = function (event) {
  ws.send("Hello");
};

ws.onmessage = function (event) {
  removeAllChildNodes(ul);
  const movies = JSON.parse(event.data);
  movies.forEach((event) => {
    const li = document.createElement("li");
    li.textContent = `- ${event.title} by ${event.author}`;
    ul.appendChild(li);
  });
};

function removeAllChildNodes(parent) {
  while (parent.firstChild) {
    parent.removeChild(parent.firstChild);
  }
}
