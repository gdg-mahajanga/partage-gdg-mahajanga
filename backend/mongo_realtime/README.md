# Use mongodb as a realtime database

## Steps

- Stop your currect mongoDB instance. If using macOS with brew, then run `brew services list` to watch available services then identify mongodb instance. Then use `brew services stop mongodb-community` (here `"mongodb-community"` is the name of service instance in my case)
- Convert your standalone MongoDB to replica sets by running this command: `mongod --port 27017 --replSet rs0`. It will starts your mongo instance as a replicaSet named as rs0

- Now, let's initiate this replication set by running this command:

```bash
$mongo
$rs.initiate()
```

- Now that replica set is running, create your database
- Now, let's create a node project by creating a folder and run `yarn init -y`, or if you are using _npm_ `npm init -y`, into it to. This will create a package.json file with default informations
- Now let's add our packages, since we will use _express_ with _mongoDB_, we need to add these packages: `yarn add express mongoose`. Open with your favorite code editor and let's begin coding!
- Let's start by creating a file named `index.js` at the root of our folder, and add these code into it:

```js
const express = require("express");
const app = express();
const mongoose = require("mongoose");
const PORT = 3000;
const MONGO_URI = "mongodb://localhost/studio";

// Connect our mongo database to our project
mongoose.connect(MONGO_URI, {});
mongoose.connection.on("open", (ref) => {
  console.log("Connected to mongo server");
  app.listen(PORT, (req, res) => console.log("Server running on port ", PORT));
});
```

- What we just did is creating a server by calling _express()_, connected to our database with mongoose and checking that mongoose is really connected then we start our server accordingly. Now, let's run this script by writing `node index.js` and if all is good, we should see this message on the console:

```bash
Connected to mongo server
Server running on port 3000
```

- Now let's add our listener. Let's create a movie schema and a model corresponding to it:

```js
// Create movie schema
const movieSchema = new mongoose.Schema({
  title: {
    type: String,
    required: [true, "Title is required"],
  },
  author: {
    type: String,
    required: [true, "Author is required"],
  },
});

// Create movie model
const Movie = mongoose.model("Movie", movieSchema);
```

- Now, let's add our listener:

```js
// Subscribe to movie stream
const movieStream = Movie.watch();

movieStream.on("change", (change) => {
  console.log(change);
});
```

- Alright, we are done! Let's test it by just changing something inside the database. If everything is fine, then you should see a prompt in the console everytime there is something going on on the database. It can be useful for printing logs for example.

- You can also specify your listener. Let's watch this example:

```js
// You can also specify what to watch
const changeStream = Movie.watch({
  $match: {
    $or: [{ title: { $exists: true } }, { author: { $exists: true } }],
  },
});
```

This suggests that we are listening to changes on data when title OR author exists
