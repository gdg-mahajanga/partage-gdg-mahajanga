const express = require("express");
const app = express();
const dotenv = require("dotenv");
const env = require("env-var");
const mongoose = require("mongoose");
const { WebSocketServer } = require("ws");

// Load env variable
dotenv.config();

// Load config in env variable
const config = {
  port: env.get("PORT").required().asPortNumber(),
  wsport: env.get("WSPORT").required().asPortNumber(),
  mongo_uri: env.get("MONGO_URI").required().asString(),
};

const { port, wsport, mongo_uri } = config;

// Init wss
const wss = new WebSocketServer({ port: wsport });

// Connect to mongodb using mongoose
const connection = mongoose.connect(mongo_uri, {});

mongoose.connection.on("open", function (ref) {
  console.log("Connected to mongo server.");
  app.listen(port, () => console.log(`Server running on port ${port}!`));
});

// Middleware - Static folder for public
app.use(express.static("public"));

// Middleware - Body parser
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Middleware - Using ejs as templating engine
app.set("view engine", "ejs");

// Create movie schema
const movieSchema = new mongoose.Schema({
  title: {
    type: String,
    required: [true, "Title is required"],
  },
  author: {
    type: String,
    required: [true, "Author is required"],
  },
});

// Create movie model
const Movie = mongoose.model("Movie", movieSchema);

const getMovieList = async (ws) => {
  movies = await Movie.find({}).exec();
  ws.send(JSON.stringify(movies));
};

// Subscribe to movie stream
const movieStream = Movie.watch();

// You can also specify what to watch
const changeStream = Movie.watch({
  $match: {
    $or: [{ title: { $exists: true } }, { author: { $exists: true } }],
  },
});

// Listen to client
wss.on("connection", function connection(ws) {
  // Create moviestream listener
  movieStream.on("change", async (change) => {
    getMovieList(ws);
  });

  // Listen to message from client
  ws.on("message", function message(data) {
    getMovieList(ws);
  });
});

// Load index page
app.get("/", async (req, res) => {
  res.render("pages/index");
});

// Add movie
app.post("/movie", (req, res) => {
  const { title, author } = req.body;
  const movie = new Movie({ title, author });
  movie.save();
  res.redirect("/");
});

// Delete movie
app.delete("/movie/:id", async (req, res) => {
  const { id } = req.params;
  await Movie.deleteOne({ _id: id }).exec();
  res.redirect("/");
});
