# Bases du SEO

Le SEO (Search Engine Optimization) appelé réferencement naturel est un ensemble de techniques de utilisés par les webmasters pour
améliorer la visibilité des sites web sur les moteurs de recherche.

## Plan

### Ce qu'est le réferencement naturel
### Vocabulaires liés au SEO
### L'importance du SEO
### Comment Google effectue le SEO 
### Comment savoir si notre site a été indexé par Google
### Pourquoi notre site n'a pas été indexé
### Que faire si notre site n'a pas été indexé
### Checklist rapide pour s'assurer d'une bonne visibilité
### Les bonnes pratiques du SEO

## Ce qu'est le réferencement naturel

## Vocabulaires liés au SEO
`index:` un répertoire maintenu par Google stockant la description et l'URL de chaque page du Web ayant subi une indexation.
`exploration`: la recherche de nouvelles pages et de pages mises à jour par les robots d'exploration de Google.
`robots d'exploration`: un robot (programme informatique) crée par Google, responsable de l'exploration des sites Web et de leur indexation.
`Googlebot`: robot d'exploration de Google.
`SEO`: terme désignant les techniques permettant d'optimiser la visibilité d'un site Web mais aussi la personne responsable de
l'application de ces techniques.

## L'importance du SEO
Le SEO permet de:
* Améliorer la visibilité de votre site web sur les moteurs de recheche
* 

## Comment Google effectue le SEO 

## Comment savoir si notre site a été indexé par Google
Pour savoir si un site a été indexé par Google, il faut taper dans la barre de recherche du navigateur ou de Google:
`site:domaine.com`. 
Par exemple, pour `wikipedia`, on aura: `site:wikipedia.com`. Si il y a des résultats qui s'affichent, alors vitre site a bien été indexé. Si une erreur s'affiche à la place, alors votre site n'a pas été indexé.

## Pourquoi notre site n'a pas été indexé
Un site n'a pas été indexé pour les raisons suivantes:
* Il s'agit d'un nouveau site, donc Google n'a pas encore eu le temps de l'explorer, donc de l'indexer
* Google a rencontré une erreur lors de l'exploration du site
* Les règles établies par le développeur empêche l'indexation
* La conception du site web rend difficile l'indexation par googlebot
* Le nombre de liens vers votre votre site est insuffisant

## Que faire si notre site n'a pas été indexé
Si tel est le cas, il faut utiliser la `Google Search Console`. On y retrouve deux choix:
* Soit on entre le nom du domaine et il nous donne un fichier TXT à mettre dans la racine du site (dans le serveur)
* Soit on entre le préfixe d'une URL et il nous donne un fichier HTML à mettre lui aussi dans la racine du site (dans le serveur)
Dans les deux cas, il faut attendre `1 jour` pour la validation du domaine ou de l'URL et il faut garder le fichier importé dans la racine du serveur. Cette validation confirme que vous êtes le propriétaire du site en question et que vous souhaitez l'indexé si ce n'est pas le cas.

## Checklist rapide pour s'assurer d'un bonne visibilité selon Google

### Mon site est-il visible sur Google ?
`L'indexation du site web sur Google est simple et gratuit. Vous n'avez généralement rien à faire sur ce sujet.` 
Faites `site:exemple.com` pour voir si votre site et ses pages ont été indexées.
Si votre site ne s'affiche pas, vérifier que vous en êtes le propriétaire via la console `Google Search Console`: http://g.co/searchconsole et envoyer votre site pour indexation à l'adresse http://google.com/webmasters/tools/submit-url.

### Le contenu du site est-il de qualité ?
`S'assurer que vous utilisateurs aient la meilleure expérience sur votre site doit être votre première priorité`.
 Les élements qui font que votre site soit unique, ait de la valeur et soit captivant. Pour plus d'informations, consultez l'article `Google Webmaster Guidelines` à l'adresse http://m.com.

### Mon entreprise locale s'affiche t-elle sur Google ?
`Google Business` est un outil gratuit et facile à utiliser vous aidant à gérer la manière dont les informations de votre entreprise s'affichent sur Google, Recherche et Maps inclus.
Ajoutez votre business et son site sur http://google.com/business.

### Mon contenu est-il rapide et facile d'accès ?
Les recherches s'effectuent de plus en plus sur les appareils mobiles. Faites en sorte que votre contenu soit adaptépour tous les appareils. 
Tester si votre site est adapté au mobile sur http://g.co/mobilefriendly.

### Le site est-il sécurisé ?
`Les utilisateurs s'attendent à une navigation sécurisée.`
Assuez-vous que votre site utilisent le protocole `HTTPS`.

### Me faut-il une aide extérieure ?
`SEOs or Search Engine Optimizers (expert SEO) sont des professionnels qui peuvent vous aider à ameliorer votre site web et à le rendre plus visible sur les moteurs de recherche.`
Suivez `Do you need an SEO` pour voir le guide de Google concernant l'embauche d'un SEO sur http://e.com

### Ressources Google
Pour plus d'astuces sur la visibilité, suivez les liens ci-essous:
Google Webmasters homepage: http://google.com/webmasters or Webmaster Academy: http://g.co/WebmasterAcademy

## Les bonnes pratiques du SEO

## Aider Google (et les internautes) à interpréter votre contenu

### S'assurer que votre site soit indexé
Verifier que votre site a été indexé en utilisant la commande mentionnée plus haut. Si ce n'est pas le cas, utilisez l'outil d'inspection d'URL de `Google Search Console`. Autorisez Googlebot à explorer les fichiers JS, CSS et les images de votre site Web.

### Créer des titres de page uniques et précis
Exemple: 
```html
<html>
<head>
    <title>Brandon's Baseball Cards - Buy Cards, Baseball News, Card Prices</title> <!-- Titre de page unique et précis -->
    <meta name="description" content="Brandon's Baseball Cards provides a large selection of
    vintage and modern baseball cards for sale.
    We also offer daily baseball news and events.">
</head>
<body>
...
```

### Contrôler vos liens de titre et vos extraits dans les résultats de recherche
L'élément `<title>` de votre page d'accueil peut indiquer le nom de votre site Web ou de votre entreprise et inclure d'autres informations importantes, comme l'emplacement physique de votre établissement, ou encore quelques renseignements relatifs au domaine ou aux services proposés.

* `Décrire avec précision le contenu de la page`
Choisissez un texte de titre lisible et évocateur du contenu de la page.

* `Créer des éléments <title> uniques pour chaque page`
Assurez-vous que chaque page de votre site possède un texte unique dans l'élément <title>. Cela permettra à Google de les différencier. Si votre site utilise des pages mobiles distinctes, n'oubliez pas d'ajouter un texte descriptif dans les éléments <title> des versions mobiles.

* `Utiliser des éléments <title> courts, mais descriptifs`
Les éléments <title> peuvent être à la fois courts et informatifs. Si le texte de l'élément <title> est trop long ou jugé moins pertinent, Google peut n'afficher qu'une partie du texte dans l'élément <title> ou générer automatiquement un lien de titre dans le résultat de recherche.

### Utiliser la balise meta description
La balise meta description d'une page fournit à Google et aux autres moteurs de recherche un récapitulatif de la page. Les balises meta description sont importantes, car Google pourrait les utiliser comme extraits pour représenter vos pages dans les résultats de recherche Google. 

* `Résumer avec précision le contenu de la page`
Rédigez une description capable d'informer et d'intéresser les internautes qui voient votre balise meta description en tant qu'extrait dans un résultat de recherche. Il n'existe pas de longueur maximale ou minimale pour le texte contenu dans une balise Meta description, mais nous vous recommandons de faire en sorte qu'il soit suffisamment long pour figurer entièrement dans la recherche Google (notez que les internautes peuvent voir des extraits de différentes tailles en fonction de leur recherche et de l'appareil qu'ils utilisent), et qu'il inclue toutes les informations pertinentes pour que les internautes sachent si la page pourra leur être utile.

* `Utiliser des descriptions uniques pour chaque page`
Une balise meta description différente pour chaque page aide les internautes et Google, en particulier dans les recherches où les internautes peuvent afficher plusieurs pages sur votre domaine (par exemple, les recherches utilisant l'opérateur site:). Si votre site comporte des milliers, voire des millions de pages, il est humainement impossible d'ajouter manuellement des balises meta description à chacune d'elles. Nous vous conseillons plutôt, dans ce cas, de générer des balises meta description automatiquement en fonction du contenu de chaque page.

### Utiliser des balises de titre pour mettre en avant le texte important

* `Imaginer que vous rédigez un plan`
Comme vous le feriez pour rédiger le plan d'un article de plusieurs pages, pensez à ce que seront les points principaux et secondaires du contenu de la page, et décidez de l'endroit où il serait judicieux d'utiliser des balises de titre.

* `Utiliser les titres avec parcimonie sur la page`
Utilisez les balises de titre avec pertinence. Un trop grand nombre de balises de titre sur une page peut nuire à la consultation du contenu et empêcher les internautes de déterminer où un sujet s'achève et où le suivant commence.

## Organiser l'arborescence de votre site

### Comprendre comment les moteurs de recherche utilisent les URL

### Importance de la navigation pour les moteurs de recherche

### Planifier votre navigation en fonction de votre page d'accueil

### Utiliser des listes de fils d'Ariane

### Créer une page de navigation simple pour les utilisateurs

### Des URL simples informent sur le contenu

### Les URL s'affichent dans les résultats de recherche

## Optimiser votre contenu

### Rendez votre site intéressant et utile

### Comprendre ce que veulent vos lecteurs (et répondre à leurs besoins)

### Agir de manière à cultiver la confiance des utilisateurs

### Indiquer clairement les sources

### Fournir une quantité de contenu appropriée pour le sujet traité

### Éviter de distraire les lecteurs avec des publicités

### Utiliser les liens judicieusement

## Optimiser vos images

### Utiliser des images HTML

### Aider les moteurs de recherche à trouver vos images

### Utiliser des formats d'image standards

## Adapter votre site aux mobiles

### Remarques
`Comprendre les différences entre les appareils`

`Smartphone :` dans ce document, les termes "mobile" ou "appareil mobile" font référence à des smartphones, par exemple des appareils équipés d'Android, ou de type iPhone ou Windows Phone. Les navigateurs mobiles sont semblables à ceux des ordinateurs, car ils peuvent afficher une bonne partie de la spécification HTML5, même si la taille de leur écran est plus petite et que leur orientation par défaut est presque toujours verticale.
`Tablette :` nous considérons que les tablettes sont des appareils à part, c'est pourquoi quand nous parlons d'appareils mobiles, nous excluons généralement les tablettes. Elles ont souvent un écran plus grand que les téléphones. Ainsi, si vous n'offrez pas de contenu optimisé pour les tablettes, les utilisateurs s'attendront à voir votre site comme sur leur ordinateur plutôt que comme sur leur smartphone.
`Téléphone multimédia :` ces téléphones sont équipés de navigateurs en mesure d'afficher des pages codées conformément aux normes XHTML et acceptent le balisage HTML5 ainsi que les scripts JavaScript/ECMAScript. Cependant, ils ne sont pas forcément compatibles avec certaines API d'extension de la norme HTML5. Cela concerne en général le navigateur de la plupart des téléphones 3G qui ne sont pas des smartphones.
`Feature phone (téléphone multifonction) :` les navigateurs de ces téléphones ne peuvent pas afficher les pages Web normales codées avec la norme HTML. Il s'agit, par exemple, des navigateurs qui affichent seulement les langages cHTML (iMode), WML, XHTML-MP, etc.
Nos recommandations concernent les smartphones, mais nous encourageons les propriétaires de sites adaptés aux téléphones multimédias et multifonctions à suivre les mêmes conseils le cas échéant.

### Choisir une stratégie mobile

### Configurer vos sites mobiles afin d'optimiser leur indexation

### Bonnes pratiques
Effectuez un test d'optimisation mobile pour vérifier si votre site Web fonctionne correctement sur les appareils mobiles d'après les critères Google.
Si vous utilisez des URL distinctes pour vos pages mobiles, testez à la fois les URL mobiles et pour ordinateur, afin de vérifier que la redirection est reconnue et peut être explorée.

## Promouvoir votre site Web

### Se renseigner sur les sites de réseaux sociaux

### Entrer en relation avec les membres de la communauté associée à votre site

## Analyser les performances de recherche et le comportement des internautes

### Analyser vos performances de recherche

### Analyser le comportement des internautes sur votre site

## Autres sources d'information
[Blog Google Search Central](https://developers.google.com/search/blog)
Recevez les dernières actualités de notre blog Google Search Central. Vous y trouverez des informations sur les mises à jour de la recherche Google, les nouvelles fonctionnalités de la Search Console, et bien plus encore.

[Forum d'aide Google Search Central](https://support.google.com/webmasters/community)
Posez des questions sur les problèmes que vous rencontrez avec votre site et trouvez des conseils pour créer des sites de haute qualité dans le forum produit destiné aux propriétaires de sites Web. De nombreux contributeurs expérimentés sont présents sur ce forum, y compris des Experts Produit et parfois des Googleurs.

[Compte Twitter de Google Search Central](https://twitter.com/googlesearchc)
Suivez-nous pour connaître les actualités et les ressources qui vous permettent de concevoir un site de qualité.

[Chaîne YouTube Google Search Central](https://www.youtube.com/c/GoogleSearchCentral)
Regardez des centaines de vidéos utiles créées pour la communauté des propriétaires de sites Web, et obtenez les réponses des Googleurs à vos questions.

[Comment fonctionne la recherche ?](http://www.google.com/insidesearch/howsearchworks/thestory/index.html)
Découvrez ce qui se passe dans les coulisses lorsque vous effectuez une recherche sur Google. Une expérience étonnante !