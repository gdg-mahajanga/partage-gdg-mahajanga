class Smartphone {
        String brand, color;
        int year;

        Smartphone(this.brand, this.color, this.year);

        String get brandValue => this.brand;
        set brandValue(String value) => this.brand = value;

        String get colorValue => this.color;
        set colorValue(String value) => this.color = value;

        int get yearValue => this.year;
        set yearValue(int value) => this.year = value;
    }

    void main() {
        var itel = Smartphone("Apple", "gold", 2021);
        print(itel.brandValue);
    }