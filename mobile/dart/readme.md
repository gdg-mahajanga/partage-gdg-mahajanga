# Dart

Dart est un langage de programmation permettant de développer des applications mobiles `cross-platform`(Android, iOS) via le framework `Flutter`. Il est développé et maintenu par Google.

## Plan

1. **Installation**
2. **Fondamentaux**
3. **Types de données**
4. **String**
5. **Conversion de types**
6. **Constantes**
7. **Null**
8. **Opérateurs**
9. **Boucles**
10. **Collections (List, Map, Set)**
11. **Fonctions**
12. **Classes**
13. **Exceptions**

## 1 - Installation

Trois options sont disponibles:

1. Installer Dart SDK 
> [https://dart.dev/get-dart](https://dart.dev/get-dart)

2. Installer Flutter (Dart SDK inclus)
> [https://docs.flutter.dev/get-started/install](https://docs.flutter.dev/get-started/install)

3. Dartpad (en ligne)
> [https://dartpad.dev](https://dartpad.dev)


## 2 - Fondamentaux
Dart est un langage:
- `Statiquement typé`
- `Compilé`
- `Orienté objet`

Deux (2) modes de compilation existent en Dart:
- `AOT` (Ahead of Time)
- `JIT` (Just In Time)

### Point d'entrée
Le point d'entrée d'un programme en Dart est la fonction:
```dart
void main() {
    // code dart
}
```

**Exemple de code**:
```dart
void main() {
    var firstName = "Jane";
    var secondName = "Doe";

    print(firstName + ' ' + secondName);
}
```
**Output**
```dart 
Jane Doe
```

### Entrée / Sortie
Pour la lecture au clavier et l'affichage à l'écran:
- Entrée: `stdin.readLineSync()`
- Sortie: `stdout.writeln("")`

**Exemple de code**
```dart
stdout.writeln("What is your name ?");
String name = stdin.readLineSync();

print("My name is $name")
```

**Output**
```
What is your name ?
Jane Doe
My name is Jane Doe
```

### Commentaires
Il y a 3 types de commentaires en Dart:
- Inline: 
```dart 
// Inline comment
```
- Block: 
```dart
/* 
Block Comment
*/
``` 
- Documentation:
```dart
/// Documentation
```

## 3 - Types de données
Il y a 5 types de bases pour les variables en Dart:
- int
- double
- String
- bool
- dynamic

### Le type: `int`
**Exemple de code**
```dart
int age1 = 22;
var age2 = 18;

print("age1: $age1 | age2: $age2");
``` 

**Output**
```
age1: 22 | age2: 18
```

### Le type: `double`
**Exemple de code**
```dart
double score1 = 85.5;
var score2 = 100.25;

print("score1: $score1 | score2: $score2");
``` 

**Output**
```
score1: 85.5 | score2: 100.25
```

### Le type: `String`
**Exemple de code**
```dart
double city1 = "Mahajanga";
var city2 = "Toamasina";

print("city1: $city1 | city2: $city2");
``` 

**Output**
```
city1: Mahajanga | city2: Toamasina
```

### Le type: `bool`
**Exemple de code**
```dart
bool isVisible1 = true;
var isVisible2 = false;

print("isVisible1: $isVisible1 | isVisible2: $isVisible2");
``` 

**Output**
```
isVisible1: true | isVisible2: false
```

### Le type: `dynamic`
**Exemple de code**
```dart
dynamic nb = 54;
print("nb: $nb\n");

nb = "Hey!";
print("nb: $nb\n")

``` 

**Output**
```
nb: 54
nb: Hey!
```

## 4 - String

 