# Introduction à Typescript

!["Typescript"](./typescript.png)

TypeScript est un langage de programmation de haut niveau gratuit et open source développé et maintenu par Microsoft. Il s'agit d'un sur-ensemble syntaxique strict de JavaScript et ajoute un typage statique facultatif au langage. Il est conçu pour le développement de grandes applications et se transpile en JavaScript.

## Plan

### Ce qu'est Typescript

### Pourquoi Typescript a été inventé

### Typescript et les navigateurs

### Quand utiliser Typescript

### Installation du compilateur Typescript

### Fonctionnalités du langage

### Ressources pour étudier Typescript

## Ce qu'est Typescript

---

Typescript est un sur-ensemble de Javascript. Cela siginifie que tout code Javascript est valide en Typescript mais pas l'inverse.

## Pourquoi Typescript a été inventé

---

**_Limites du Javascript_**

-   Javascript permet d'appeler une fonction sans exiger de lui passer un argument même si cette dernière en nécessite un.
-   L'erreur ne sera détécté qu'une fois lancé dans le navigateur.

```ts
function getLowerCaseString(arg) {
    return arg.toLowerCase();
}

getLowerCaseString(); // Erreur détéctée une fois le code lancé
```

**_Avantages de Typescript_**

-   Typescript permet d'appeler une fonction en exigeant de lui passer un argument si cette dernière en nécessite un.
-   Les erreurs sont détéctées au moment de l'écriture du code.
-   Typescript exige de passer le type du paramètre à passer dans la fonction.

```ts
function getLowerCaseString(arg: string) {
    return arg.toLowerCase();
}

getLowerCaseString("Hello World"); // Cela marche
getLowerCaseString(200); // Erreur détectée au moment de l'écriture du code
```

Typescript:

-   permet de définir les types static de nos variables, fonctions, ... (ce qui n'est pas le cas de Javascript)
-   permet une meilleure documentation du code (facile à comprendre)
-   vérifie que notre code fonctionne bien (constater les erreurs durant le développement)
-   permet d'écrire un code plus sûr et plus clean (il faut quand même tester votre code)

## Typescript et les navigateurs

---

Les navigateurs ne comprenent pas Typescript. Il faut compiler le code Typescript en Javascript via un compilateur.

## Quand utiliser Typescript

---

-   Pour les grands projets
-   Pour les travaux en équipe
-   Pour plus de sécurité

## Installation du compilateur Typescript

---

Pour installer globalement le compilateur Typescript, il faut taper la commande suivante dans le terminal:

`npm install -g typescript`

Pour compiler un fichier, utilisez la commande suivante:

`tsc <nom_fichier> -w`

## Fonctionnalités du langage

---

### Variables, boucles et conditions

Elles sont les mêmes qu'en Javascript.

```ts
let firstName: string = "Ianel";

let friends: string[] = ["Michael", "Johny"];

for (const friend of friends) {
    console.log(friend);
}

if (typeof firstName == "string") {
    console.log(`Hello ${firstName}`);
}
```

### Types

-   Primitifs: `string`, `number`, `boolean`:

```ts
let lastName: string = "Tombozafy";
let age: number = 24;
let isMale: boolean = true;
```

-   `any`:

Le type `any` désactive la vérification de types en Typescript. C'est à votre risque et péril. On peut y stocker n'importe quel type.

```ts
let obj: any = { x: 0 };

// Il n'y a pas d'erreurs
obj.foo();
obj();
obj.bar = 100;
obj = "Hello";
```

-   `unknown`:

Le type `unknown` accepte tous les types. Mais il faut d'abord vérifier le type.

Avec `any`:

```ts
let inputData: any;
inputData = true;
console.log(typeof inputData); // boolean

let inputAge: number;
inputAge = inputData; // Ok
console.log(typeof inputAge); // boolean
```

Avec un `unknown`:

```ts
let inputData: unknown;
inputData = 20;
console.log(typeof inputData); // number

let inputAge: number;

if (typeof inputData === "number") {
    inputAge = inputData; // Ok
    console.log(typeof inputAge); // number
}
```

-   Array:

```ts
let list: number[] = [1, 2, 3];
// OU
let list: Array<number> = [1, 2, 3];

let pays: (string | boolean | number)[] = ["Madagascar", 401, true];
// OU
let pays: Array<string | number | boolean> = ["Madagascar", 401, true];
// OU
let pays: any[] = ["Madagascar", 401, true];
```

-   Tuple:

Il n'existe qu'en Typescript mais pas en Javascript. Un tuple est un tableau qui a une longueur définie et dont le type des contenus est connu à l'avance.

```ts
let person: [string, string, number];
person = ["Tombozafy", "Ianel", 24];
```

La méthode `push` permet de déroger à cette règle.

```ts
person.push(56);
person.push("Ragnar");
```

Utilisation:

```ts
const member: { level: [string, number] } = {
    level: ["admin", 1],
};
```

-   Objet

```ts
let car: {
    color: string;
    date: number;
    speed: number;
} = {
    color: "red",
    date: 2022,
    speed: 120,
};
```

-   `void`:

Ce type illustre l'absence de types. Il est souvent utilisé comme type de retour des fonctions qui ne retournent rien.

```js
function warnUser(): void {
    console.log("Be careful. You're warned");
}

// OU

const warnUser = (): void => console.log("Just a string");
```

-   Function:

On peut utiliser le type générique `Function` ou préciser le type des paramètres et de la valeur retournée.

```js
function sum(num1: number, num2: number): number {
    return num1 + num2;
}

let mySum: Function = sum;
console.log(mySum(20, 3)); // 23

let add: (num1: number, num2: number) => number = sum;
console.log(add(20, 3)); // 23
```

-   Paramètres facultatifs :

Ils doivent se placer après tous les paramètres obligatoires

```js
const buildName: (firstName: string, lastName?: string) => string = (
    firstName: string,
    lastName?: string
) => {
    return lastName ? `${firstName} ${lastName}` : `${firstName}`;
};

console.log(buildName("Ianel", "Tombozafy")); // Ianel Tombozafy
console.log(buildName("Michael")); // Michael
```

-   Paramètres par défaut

Ils peuvent se placer n'importe où.

```ts
function sayHi(firstName: string, lastname = "Stark"): string {
    return `Hi, ${firstName} ${lastName}`;
}

console.log(sayHit("Ianel")); // Hi, Ianel Stark
console.log(sayHi("Tony", "Starkinson")); // Hi, Tony Starkinson

function sayHi2(firstName = "Will", lastname: string): string {
    return `Hi, ${firstName} ${lastName}`;
}

console.log(sayHi2(undefined, "Smith")); // Hi, Will Smith
```

-   Rest:

Il permet de gérer plusieurs paramètres assignés à une fonction.

```ts
function buildName(firstName: string, ...restOfName: string[]): string {
    return `${firstName} ${restOfName.join(" ")}`;
}

console.log(buildName("Ianel", "Tombozafy", "Stark")); // Ianel Tombozafy Stark
```

-   Callback function type:

Il s'agit du type des fonctions callback.

```js
function setMyAge(birthYear: number, displayAge: (param: number) => void) {
    const age = new Date(Date.now()).getFullYear() - birthYear;
    displayAge(age);
}

setMyAge(1999, (num) => console.log(num));
```

-   Union Types:

Il permet d'assigner plusieurs types à une variable.

```ts
let firstName: string | number = "Roger";
firstName = 23; // Ok
```

Avec une fonction, il faut faire une vérification:

```ts
function concatenate(arg1: number | string, arg2: number | string) {
    let result;

    if (typeof arg1 === "number" && typeof arg2 === "number") {
        result = arg1 + arg2;
    } else if (typeof arg1 === "string" && typeof arg2 === "string") {
        result = arg1 + arg2;
    } else {
        result = arg1.toString() + arg2.toString();
    }

    return result;
}

console.log(concatenate(10, 25));
console.log(concatenate(10, "Hello"));
console.log(concatenate("World", 25));
```

-   Type Aliases:

Permet de définir un type particulier obtenu à partir d'autres types pour faciliter la compréhension du code.

```js
type NumStr = number | string;

type ObjUser = {
    id: number,
    userName: NumStr,
};

type ObjInvoice = {
    name: string,
    price: number,
};

const invoice = (productName: ObjInvoice, user: ObjUser) => {
    console.log(`
        Produit: ${productName.name}
        Prix: ${productName.price} MGA
        Id du client: ${user.id}
        Nom du client: ${user.userName}
    `);
};

invoice(
    { name: "Formation PHP", price: 45000 },
    { id: 124, userName: "Jane#0456" }
);
```

-   Literal types:

Permet de définir les valeurs exactes acceptées par une variable.

```ts
type Roles = "admin" | "moderator" | "normal";

let role: Roles;

role = "admin"; // OK
role = "client"; // Erreur

// Appel API
const response = await fetch("call/to/api");
const user = await response.json();

if (user.role === "admin") {
    role = "admin";
} else {
    role = "normal";
}
```

-   `never`:

Il représente le type de valeurs qui ne se produisent jamais. Le type `never` est assignable à chaque type. Mais aucun type n'est assignable à un type `never`, même le type `any`.

```ts
function throwError(errMsg: string): never {
    throw new Error(errMsg);
}

function fail() {
    return throwError("Erreur mon ami");
}

const sayHello = function (textMsg: string) {
    while (true) {
        console.log(textMsg);
    }
};
```

-   `null` et `undefined`:

`null` signale l'absence de valeur tandis que `undefined` signale une valeur non initialisée.

```ts
let age: number;
console.log(age); // undefined

let score: null = null;
console.log(score); // null
```

Pour signifier à Typescript qu'une valeur est nullable, on peut faire comme suit:

```ts
if (user?.firstName) {
    console.log(user?.firstName);
}
```

Et pour lui faire comprendre qu'une valeur est non nulle, alors on peut procéder de la manière suivante:

```ts
const person = JSON.parse(localStorage.getItem("person")!);
```

-   Type assertion:

Permet de spécifier le type particulier d'une variable lorsque le développeur le souhaite.

1ère forme:

```ts
let someValue: unknown = "This is a string";
let strLength = (someValue as string).length;
```

2ème forme:

```ts
let strLength = (<string>someValue).length;
```

## Classes

---

Les classes en Typescript sont à peu près identiques à ceux en Javascript mais il y a des différences.

```ts
class Invoice {
    product: string;
    name: string;
    price: number;

    constructor(name: string, product: string, price: number) {
        this.name = name;
        this.product = product;
        this.price = price;
    }

    getDetails() {
        console.log(`Invoice infos:
            User: ${this.name},
            Product: ${this.product}
            Price: ${this.price}
        `);
    }
}

const invoice1 = new Invoice("Tomas", "JS", 4500);
console.log(invoice1.getDetails());
```

### Héritage et polymorphisme

Les principes sont les mêmes qu'en Javascript avec l'usage d'identificateurs pour les propriétes et les méthodes.

---

```ts
class Mother {
    public name: string;
    private hair: string;
    protected eyes: number;

    constructor(name: string, hair: string, eyes: number) {
        this.name = name;
        this.hair = hair;
        this.eyes = eyes;
    }

    // Getter
    getHair() {
        return this.hair;
    }

    // Setter
    setHair(newHair: string) {
        this.hair = newHair;
    }

    speak() {
        console.log(` I am ${this.name} and my hair are ${this.hair} `);
    }
}

class Children extends Mother {
    speak() {
        super.speak();
    }

    speakFrench() {
        console.log(
            `Je suis ${this.name} et mes cheveux sont ${this.getHair()}`
        );
    }

    // Getter
    getEyes() {
        return this.eyes;
    }
}

const mother = new Mother("Elia", "brun", 2);
const child = new Children("Eliana", "marron", 2);
```

Le constructeur peut s'écrire aussi:

```ts
class Mother {
    constructor(
        public name: string,
        private hair: string,
        protected eyes: number
    ) {}
}
```

### Readonly

Il est possible de mettre des propriétés en lecture seule. Elles doivent être initialisées au moment de leur déclaration ou via l'instanciation.

```ts
class Octopus {
    readonly name: string;
    readonly numberOfLegs: number = 8;

    constructor(theName: string) {
        this.name = theName;
    }
}

let octDad = new Octopus("Man with the octopus face");
octDad.name = "Octopus Man"; // Erreur
```

## Intefaces

Les interfaces n'existent pas en Javascript mais elles sont présentes en Typescript.

-   Pour définir le type d'un objet

```js
interface Person {
    firstName: string;
    talk: () => string;
}

let ianel: Person = {
    firstName: "Ianel",
    talk() {
        return `Je m'appelle ${this.firstName}`;
    },
};

// Erreur
let toto: Person = {
    firstName: "toto",
    talk(word: string) {
        return `Je dis: ${word}`;
    },
};
```

-   Pour définir le type d'une classe

```js
interface ClockTime {
    currentTime: Date;
    giveCurrentTime: () => string;
}

class Clock implements ClockTime {
    public currentTime: Date;

    constructor(currentTime: Date) {
        this.currentTime = currentTime;
    }

    giveCurrentTime() {
        return `It is now ${this.currentTime}`;
    }
}

const myClock = new Clock(new Date(Date.now()));
console.log(myClock);
```

## Resources pour étudier Typescript:

-   [https://www.typescriptlang.org](https://www.typescriptlang.org): Documentation officielle
-   [Youtube](https://www.youtube.com/playlist?list=PLjwdMgw5TTLX1tQ1qDNHTsy_lrkCt4VW3): Chaîne Youtube Grafikart
