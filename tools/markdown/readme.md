# Découverte de Markdown

Le format **_markdown_** est un format de fichier qui permet une mise en forme simple et rapide grâce à des balises de la même manière que le langage HTML. L'extension d'un fichier markdown est `.md`.

---

## Titre

Dans un fichier markdown, les titres sont balisés par des hashtags (#) au début. De la même manière que le HTML, il y a 6 niveaux de titres.

**Forme:**

```md
# h1

## h2

### h3

#### h4

##### h5

###### h6
```

**Résultat:**

# h1

## h2

### h3

#### h4

##### h5

###### h6

---

## Gras

Pour mettre du texte en gras, on utilise deux astérisques ou deux underscores comme balises.

**Forme:**

```md
**gras**
```

**Résultat:**

**Gras**

---

## Italique

La mise en forme en italique se fait grâce à un astérisque ou à un underscore.

**Forme:**

```md
_italique_
```

**Résultat:**

_italique_

---

## Blockquote

Une citation ou blockquote se met en forme grâce à un chevron droit.

**Forme:**

```md
Grace Hopper a dit:

> Je suis plus interéssé par le futur que par le passé.
```

**Résultat:**

Grace Hopper a dit:

> Je suis plus interéssé par le futur que par le passé.

---

## Liste ordonnée

Une liste ordonnée est de la forme:

**Forme:**

```md
1. Un
2. Deux
3. Trois
```

**Résultat:**

1. Un
2. Deux
3. Trois

---

## Liste non ordonnée

**Forme:**

```md
- Premier element
- Second element
- Troisième element
```

**Résultat**:

- Premier element
- Second element
- Troisième element

---

## Code

**Forme:**

```md
`const a = 1;`
```

**Résultat:**

`const a = 1;`

---

## Code dans un langage spécifique

**Forme:**

````md
```dart
dynamic age = 25;
```
````

**Résultat:**

```dart
dynamic age = 25;
```

## Ligne horizontale

**Forme:**

```md
---
```

**Résultat:**

---

---

## Liens

**Forme:**

```md
[Page d'accueil Facebook](http://web.facebook.com)
```

**Résultat**

[Page d'accueil Facebook](http://web.facebook.com)

---

## Image

**Forme:**

```md
![alt text](url)
```

**Exemple:**

```md
![Markdown Cheatsheet](markdown-cheat-sheet-basic-elements.jpeg)
```

**Résultat:**

![Markdown Cheatsheet](markdown-cheat-sheet-basic-elements.jpeg)

## Tableau

**Forme:**

```md
| Titre 1 | Titre 2 |
| ------- | ------- |
| Ligne 1 | Ligne 1 |
| Ligne 2 | ligne 2 |
```

**Résultat:**

| Titre 1 | Titre 2 |
| ------- | ------- |
| Ligne 1 | Ligne 1 |
| Ligne 2 | ligne 2 |

## Block de code

**Forme:**

````md
```
{
    "nom": "Doe",
    "prenom: "John",
    "age": 42
}
```
````

**Résultat:**

```
{
    "nom": "Doe",
    "prenom: "John",
    "age": 42
}
```

## Réference ou Note de bas de page

**Forme:**

```md
La pandémie de COVID est en récession selon l'OMS[^1]
[^1]: [OMS article N°2342/2022](http://www.who.org)
```

**Résultat:**

La pandémie de COVID est en récession selon l'OMS[^1]

[^1]: [OMS article N°2342/2022](http://www.who.org)

## Liste de définition

**Forme:**

```md
terme
: définition
```

**Résultat:**

Soleil
: Etoile de type naine jaune au centre du système solaire

## Liste de tâches

**Forme:**

```md
- [x] Apprendre le markdown
- [] Faire la présentation sur le markdown
```

**Résultat:**

- [x] Apprendre le markdown
- [ ] Faire la présentation sur le markdown

## Texte barré

**Forme:**

```md
~~La terre est plate~~
```

**Résultat:**

~~La terre est plate~~

![Markdown Cheatsheet Extended](markdown-cheatsheet-extended-syntax.jpeg)
