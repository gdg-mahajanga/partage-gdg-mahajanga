<img
  src="git.png"
  alt="git logo"
  style="display: block; margin-left: auto; margin-right: auto; width: 50%;"
/>

## Git ?

Git est un outil qui permet de retracer l'historique des modifications sur un essemble de fichier donné, il s'agit d'un logiciel de "versioning system".  
Il est généralement utilisé pour la collaboration d'une équipe sur le code source d'un logiciel.

### Un peu d'histoire
Git a été initié par Linus Trovalds en 2005 pour le développement de la Linux Kernel, puis maintenu et développé par d'autres développeurs du même projet.    
Logciel libre et open source, Git est sous la licence  [GPL-2.0-only](https://en.wikipedia.org/wiki/GNU_General_Public_License). 

### Pourquoi Git et pas un autre systeme de gestion de version
Les points qui differencient Git sont:  
✅ sa performance et rapidité  
✅ sa nature distribuée  

## Installation

### WINDOWS

Installer en standalone

- [32-bits Git](https://github.com/git-for-windows/git/releases/download/v2.35.1.windows.2/Git-2.35.1.2-32-bit.exe)
- [34-bits Git](https://github.com/git-for-windows/git/releases/download/v2.35.1.windows.2/Git-2.35.1.2-64-bit.exe)  

Installer portable
- [32-bits Git](https://github.com/git-for-windows/git/releases/download/v2.35.1.windows.2/PortableGit-2.35.1.2-32-bit.7z.exe)
- [34-bits Git](https://github.com/git-for-windows/git/releases/download/v2.35.1.windows.2/PortableGit-2.35.1.2-64-bit.7z.exe)

### LINUX
Sur une debian based distro:
```bash
sudo apt install git
```
Pour verifier que l'installation a bien été faite, rendez-vous dans votre terminal et entrez-y la commande suivante pour récuperer la version installée:  

```base
git --version
```

## Git workflow

<img
  src="flow.png"
  alt="git logo"
  style="display: block; margin-left: auto; margin-right: auto; width: 100%;"
/>

### Place a la pratique

Pour commencer, vous allez recupérer une repository déja existante, qui n'est autre que notre repository de partage.  

Rendez vous dans votre terminal et naviguez dans le dossier où vous souhaiteriez placer le projet. Tapez ensuite la commande suivante:  

```bash
git clone https://gitlab.com/gdg-mahajanga/partage-gdg-mahajanga.git
```

Après, toujours depuis votre terminal, naviguez vers le dossier "partage-gdg-mahajanga".  

Indiquez a Git votre identité en tapant respectivement:  
```bash
git config user.name "<votre_nom>"
```
```bash
git config user.email "<votre email>"
```
Pour ce repository, ce sont les informations que Git attribuera a vos commit.
#### Git branch

Git fonctionne en terme de branch pour cloisonner les modifications.  
Quand on travail avec Git, on se situe dans une branch.  
Une branch a une autre branch pour branch parente et s'initialise donc avec les modifications qu'a la branch parente.  

- Pour voir la liste des branch existantes:  
```bash
git branch
```
La branch dans laquelle on se situe est celle avec un asterisque (*).
- Pour créer une nouvelle branch:
```bash
git branch <nom_de_ma_branch>
```
La branch parente de la nouvelle branch est celle dans laquelle on se situe lors de la creation de la nouvelle branch.
- Pour changer de branch:
```bash
git checkout <nom_de_ma_branch>
```

#### Faire des modifications
Lorsqu'on modifie son code depuis son éditeur, les modifications se trouve dans la working directory.

- Pour voir la liste des modifications dans la working directory:  
```bash
git status
```
- Pour voir les details des modifications dans la working directory:  
```bash
git diff
``` 

Ensuite il faut amener les modifications dans la staging area.  
C'est une étape intermédiaire pour indiquer à Git la liste des modifications à embarquer dans le prochain commit.  
- Pour prendre la liste des modifications à embarquer vers la staging area:  
```bash
git add .
```
Le "." signifie "ajoute toute les modifications". Alternativement, pour un ajout plus sélectif, on peut faire :  
```bash
git add <chemin_vers_le_fichier_modifié>
``` 

Pour enfin prendre en considération les modifications dans la commit history de son local repo, il faut commit:  

```bash
git commit -m "<mon_message_de_commit>"
```
Maintenant, les modifications sont dans un commit, dans la branch actuelle.  
Pour partager les modifications dans une autre branch, il faut d'abord sans rendre dans cette autre branch:  
```bash
git checkout <la_branch_qui_veut_les_nouvelles_modifs>
```
Ensuite on merge la branch avec les modifications dans cette branch:
```bash
git merge <nom_de_la_branch_avec_les_modifs>
```

#### Partager les modifications vers la remote repository

- Pour partager les modifications avec la remote repository, il faut push notre branch vers cette dernière :  
```bash
git push origin <nom_de_ma_branch>
```
"origin" est le nom par defaut attribué a un remote repository quand on clone.  
- Pour mettre nos modifications sur une branch dans la remote repository, il faut créer une merge request sur celle-ci.

### Recap

Voila donc, nous avons:

- clone un repo depuis gitlab
- créer notre branch
- apporter des modifications en créant un commit dans notre branch
- partager nos modifications sur la remote repository

#### Mais ce n'est pas tout

- On peut parametrer ses identifiants de maniere globale
- On peut créer directement un local repository sans la cloner depuis une remote repository
- On peut ensuite lui attribuer une remote repository
- On peut avoir plusieur remotes
- On peut changer la branch parente d'une branch donnée meme si cette derniere a deja des modifications/commit
- On peut choisir d'injecter une selection de commit vers une branch et non toujour une branch entiere
- Git ne gere pas magiquement tous les cas de modifications, certain cas sont necessite une intervention.
- et ce n'est pas encore tout non plus, the linux kernel team is among the smartest people known by the tech industry
