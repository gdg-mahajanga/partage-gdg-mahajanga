<img src="horizontal-logo-monochromatic-white.png" alt="docker logo" style="display: block; margin-left: auto; margin-right: auto; width: 50%;" />

## C'est quoi ?

>  Docker est un outil qui peut empaqueter une application et ses dépendances dans un conteneur isolé, qui pourra être exécuté sur n'importe quel serveur.

##  Docker outil incontournable dans l'industrie ?

Docker permet ainsi la :  
✅ portabilité  
✅ la prévisibilité  
✅ la reproductibilité.

En separant le niveau applicatif de l'infrastructure, docker simplifie et fluidifie le **développement**, le **test** et le **déployement** d'application.

Pour une version plus detaillée de ce qu'est docker, je vous invite a check la 👉 [doc officielle](https://docs.docker.com/get-started/overview/ ) 👈.

## Overview de l'architecture et terminologies clées
<img src="architecture.svg" alt="docker architecture" style="display: block; margin-left: auto; margin-right: auto; width: 100%;" />

Ce qu'il faut retenir :

- Les images sont stockées sur une registery ([Docker hub ](https://hub.docker.com/) en est un)
- On pull les images depuis une registery vers un host
- On build un container depuis une image
- On run le container pour le lancer


## Installation

Pour l'installation, je vous invite à suivre le lien et à suivre étape par étape les intructions en fonction de votre OS : 🛠️[the future is built here](https://docs.docker.com/get-docker/)🛠️

## Prise en main
Une fois l'installation terminée, executez cette commande dans votre terminal.
```bash
docker run -d -p 80:80 docker/getting-started
```
Rendez-vous aux 👉 [magic here](http://127.0.0.1:80) 👈.

### Qu'a fait cette commande ?

- `docker` => docker client appel docker engine pour executer quelque chose
- `run` => on va runner une image
- `-d` => docker s'il te plait run l'image en detached mode (en daemon)
- `-p 80:80` => relie le port 80 de du host, en l'occurence ma machine machine de guerre, au port 80 du container
- `docker/getting-started` => l'image a runner

## Containeriser une application avec Docker

### L'application en question
 Quoi de mieux qu'une todo app pour une prise en main, télechager le 👉 [ici](http://127.0.0.1/assets/app.zip) 👈 . 
 Il s'agit d'une todo app qui est fait avec node.js (nule besoin de connaitre node.js pour poursuire notre découverte de docker!). 
 
 Une fois extracté, ouvrez le dans votre code editor et vous devriez voir cette arborescence.

<img src="app.png" alt="app overview" style="display: block; margin-left: auto; margin-right: auto; width: 50%;" />

### Construisons l'image du container de l'application

Pour ce faire nous aurons besoin d'un `Dockerfile`. Il s'agit d'un fichier contenant les intructions pour build notre container image.

1. Créez une fichier appelé `Dockerfile` (attention, il ne faut pas mettre .txt a la fin!) à la racine du dossier de votre app todo. Ensuite mettez-y ce contenu.

``` Dockerfile
FROM node:12-alpine
# Adding build tools to make yarn install work on Apple silicon / arm64 machines
RUN apk add --no-cache python2 g++ make
WORKDIR /app
COPY . .
RUN yarn install --production
CMD ["node", "src/index.js"]
```

Voyons un peu le contenu de notre `Dockerfile`.

* ``` Dockerfile 
  FROM node:12-alpine
  ```
  -> Cette ligne indique l'image de base à partir de laquelle on va construire notre propore image
* ``` Dockerfile 
  RUN apk add --no-cache python2 g++ make
  ```
  -> Instruction qui lance une commande linux pour installer une dépendence utile à notre image
* ``` Dockerfile 
  WORKDIR /app
  ```
  -> Indique le working directory pour les éventuelles intructions ``RUN``, ``CMD``, ``ENTRYPOINT``, ``COPY`` et ``ADD`` du Dockerfile
* ``` Dockerfile 
  COPY . .
  ```
  -> Copie tout depuis le dossier de notre app vers la working directory du container
* ``` Dockerfile 
  RUN yarn install --production
  ```
  -> Instruction qui lance une commande propre à node.js qui va installer les dépendences de l'application
* ``` Dockerfile 
  CMD ["node", "src/index.js"]
  ```
  -> La commande par défaut à lancer quand le container démarre

2. Maintant rendez-vous dans votre terminal, à la racine du dossier où se trouve le `Dockerfile` et construison l'image.

```bash
docker build -t todo-app .
```
💡 Le flag `-t` permet de tager l'image, ainsi on le retrouvera facilement parmis notre liste d'image.

### Lancons notre container

1. 
```bash
docker run -d -p 3000:3000 todo-app
```
La commande nous est familière, `-d` pour detached mode et `-p 3000:3000` pour bind les ports !

2. Allons voir! [http://127.0.0.1:3000](http://127.0.0.1:3000)

### Et voila!

- [x] Un premier appercu de docker
- [x] Les basics pour construire une image docker a partir d'un Dockerfile
- [x] Lancement du container

Mais ce n'est pas tout. Il y a encore tant a decouvrir.

Lancez votre container `getting-started` et continuez à découvrir.
```bash
docker run -d -p 80:80 docker/getting-started
```

Quand vouv vous sentirez plus a l'aise, référez vous à la 👉 [doc officielle](https://docs.docker.com/reference/) 👈 .


### Quelques commandes de tous les jours

- Pour voir la liste des images
```bash
docker image ls
```

- Pour voir la liste des containers
```bash
docker container ls
```

- Pour start un container existant
```bash
docker container start <your_container_id_or_name>
```

- Pour stoper un container existant
```bash
docker container stop <your_container_id_or_name>
```

- Pour restart un container existant
```bash
docker container restart <your_container_id_or_name>
```

- Pour supprimer un container existant
```bash
docker rm container <your_container_id_or_name>
```

- Pour accéder à l'interieur d'un container
```bash
docker exec -it <your_container_id_or_name> /bin/sh
```

